# latam_search

Fazer busca no Latam es_CL

## Preparando o ambiente

### Linux
```sh
#abre o terminal e instale virtualenv e use o comando pra criar a pasta
$ git clone https://gitlab.com/erickson_douglas/latam_search.git && cd latam_search
$ virtualenv -p python3 venv && source venv/bin/activate
$ pip install -r requirements.txt
```
### Windows
#### [Python3]( https://dicasdepython.com.br/como-instalar-o-python-no-windows-10/)
#### [latam_search](https://gitlab.com/erickson_douglas/latam_search/-/archive/master/latam_search-master.zip)
extrair o zip
```sh
# abre o cmd
$ cd latam_search
$ pip install -r requirements.txt
```
OR
```sh
# abre o cmd
$ pip install requests PyYAML babel
```

## Exemplos
### Consulta pela data
``` sh
$ python3 latam.py
```
