from csv import DictWriter, DictReader
from datetime import timedelta
from requests import Session
from time import sleep, time
from babel.numbers import format_currency
from re import findall
from functools import reduce


class Latam:
    def __init__(self, params, tm):
        self.host = 'http://bff.latam.com'
        self.base = 'http://latam.com'
        self.url = 'https://www.latam.com/es_cl/apps/personas/booking?'
        self.params = params
        self._fights = None
        self._total = 0
        self.time = tm
        self.session = Session()

    @staticmethod
    def _money(value):
        if value:
            return format_currency(value[0], value[1], locale="pt_BR")
        return value

    def _import_csv(self, crawler):
        fieldnames = ['cod', '1_data_partida', '1_hora_partida', '1_local_partida',
                      '1_data_chegada', '1_hora_chegada', '1_local_chegada', '1_conexao',
                      '2_data_partida', '2_hora_partida', '2_local_partida',
                      '2_data_chegada', '2_hora_chegada', '2_local_chegada', '2_conexao',
                      'Promo_E', 'Promo_E_m', 'Basic_E', 'Basic_E_m', 'Light_E', 'Light_E_m', 'Plus_E', 'Plus_E_m',
                      'Top_E', 'Top_E_m', 'Plus_P', 'Plus_P_m', 'Top_P', 'Top_P_m', 'url']
        if self.params.get('old'):
            try:
                with open(f'csv/{crawler}__consulta_{self.params["dt_from"]}_'
                          f'{self.params.get("dt_to","0000-00-00")}.csv', newline='') as read:
                    self.params['old'] = list(map(lambda x: x, DictReader(read)))
            except FileNotFoundError:
                del self.params['old']

        _name_file = open(f'csv/{crawler}__consulta_{self.params["dt_from"]}_'
                          f'{self.params.get("dt_to", "0000-00-00")}.csv', 'w', newline='')
        self._fights = DictWriter(_name_file, fieldnames)
        self._fights.writeheader()
        if self.params.get('old'):
            list(map(self._fights.writerow, self.params['old']))
            self.params['old'] = list(map(lambda x: x['cod'], self.params['old']))
            self._total = len(self.params['old'])

    def list_destiny(self):
        _list = findall(r'<a class="dropdown-item" href="/vamos/(\w*)/">',
                        self.session.get(f'{self.base}/vamos/es_cl/destinos/').text)
        _list.append('es_cl')
        return _list

    def list_origin(self, local='es_cl'):
        return list(map(
            lambda x: x["airportCode"],
            self.session.get(url=f"{self.host}/latamapiflightsearch/v1/{local.replace('_','/')}/origins").json()
        ))

    def list_filter(self, file='filter'):
        from yaml import load, CFullLoader
        _result = {}
        try:
            with open(f'filters/{file}.yaml') as f:
                _file = load(f, Loader=CFullLoader)
                if _file.get('origem'):
                    _result['origem'] = _file['origem']
                else:
                    _result['origem'] = self.list_origin(local='es_cl')
                if _file.get('destino'):
                    _result['destino'] = _file['destino']
                else:
                    _result['destino'] = reduce(lambda x, y: x+y, map(self.list_origin, self.list_destiny()))
        except Exception as e:
            print("arquivo invalido ou não encontrado")
            print("Erro:",e)
            return None
        return _result

    def info_markers(self, origin):
        params = {'origin': origin, 'from': self.params['dt_from']}
        if self.params.get('dt_to'):
            params['to'] = self.params['dt_to']
        _r = self.session.get(params=params,
                              url=f"{self.host}/latamapiflightsearch/v1/es/cl/markers").json().get('infoMarkers')
        if _r:
            self.params['origin'] = origin
            _markers = list(map(lambda x: x['airportCode'], _r))
            list(map(self.recommended_flight, _markers))

    def info_markers_to_destination(self, origin):
        print(f'Procurando em {origin}')
        self.params['origin'] = origin
        list(map(self.recommended_flight, self.params['destiny']))

    def recommended_flight(self, destination):
        if self.params['origin'] == destination:
            return None
        elif self.params.get('old'):
            if f'{self.params["origin"]} - {destination}' in self.params['old']:
                return None

        _fight = {}
        params = {'departure': self.params['dt_from'],
                  'origin': self.params['origin'],
                  "destination": destination,
                  'language': "ES",
                  "country": "CL",
                  "adult": "1"}
        if self.params.get('dt_to', None):
            params['return'] = self.params['dt_to']
            _r = self.session.get(url=f"{self.host}/ws/proxy/booking-webapp-bff/v1/public/revenue/bestprices/outbound",
                                  params=params).json().get("itinerary", {"routeType": "CROSS_DOM"})
            sleep(self.time)
            if not _r.get("routeType") == "CROSS_DOM":
                _fight.update({'cod': f'{self.params["origin"]} - {destination}',
                               '1_conexao': _r['originDestinations'][0]['stops'],
                               '2_conexao': _r['originDestinations'][1]['stops']})
                _departure = _r['originDestinations'][0]["departure"]
                _arrival = _r['originDestinations'][0]["arrival"]
                _datetime1 = _departure["timestamp"].split("T")
                _datetime2 = _arrival["timestamp"].split("T")
                _fight.update({'1_data_partida': _datetime1[0], '1_hora_partida': _datetime1[1].split('-')[0],
                               '1_local_partida': f'{_departure["airport"]} - {_departure["airportName"]}'
                                                  f' - {_departure["country"]}',
                               '1_data_chegada': _datetime2[0], '1_hora_chegada': _datetime2[1].split('-')[0],
                               '1_local_chegada': f'{_arrival["airport"]} - {_arrival["airportName"]}'
                                                  f' - {_arrival["country"]}'})
                _departure = _r['originDestinations'][1]["departure"]
                _arrival = _r['originDestinations'][1]["arrival"]
                _datetime1 = _departure["timestamp"].split("T")
                _datetime2 = _arrival["timestamp"].split("T")
                _fight.update({'2_data_partida': _datetime1[0], '2_hora_partida': _datetime1[1].split('-')[0],
                               '2_local_partida': f'{_departure["airport"]} - {_departure["airportName"]}'
                                                  f' - {_departure["country"]}',
                               '2_data_chegada': _datetime2[0], '2_hora_chegada': _datetime2[1].split('-')[0],
                               '2_local_chegada': f'{_arrival["airport"]} - {_arrival["airportName"]}'
                                                  f' - {_arrival["country"]}'})
                del _departure, _arrival, _r, _datetime1, _datetime2
                _r = self.session.get(
                    url=f"{self.host}/ws/proxy/booking-webapp-bff/v1/public/revenue/recommendations/outbound",
                    params=params).json().get("data")
                sleep(self.time)
                _f = _r[0]["flights"][0]["cabins"]
                if len(_f) > 1:
                    _economy = dict(
                        map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                       x['pricesPerPassenger']['adult']['currency'])), _f[0]['fares']))
                    _business = dict(
                        map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                       x['pricesPerPassenger']['adult']['currency'])), _f[1]['fares']))
                else:
                    if _f[0]['code'] == 'Y':
                        _economy = dict(
                            map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                           x['pricesPerPassenger']['adult']['currency'])),
                                _f[0]['fares']))
                        _business = {}
                    else:
                        _economy = {}
                        _business = dict(
                            map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                           x['pricesPerPassenger']['adult']['currency'])),
                                _f[0]['fares']))

                _fight.update({"Promo_E": _economy.get("PROMO", '  ')[0], "Promo_E_m": _economy.get("PROMO", '  ')[1],
                               "Basic_E": _economy.get("BASIC", '  ')[0], "Basic_E_m": _economy.get("BASIC", '  ')[1],
                               "Light_E": _economy.get("LIGHT", '  ')[0], "Light_E_m": _economy.get("LIGHT", '  ')[1],
                               "Plus_E": _economy.get("PLUS", '  ')[0], "Plus_E_m": _economy.get("PLUS", '  ')[1],
                               "Top_E": _economy.get("TOP", '  ')[0], "Top_E_m": _economy.get("TOP", '  ')[1],
                               "Plus_P": _business.get("PLUS", '  ')[0], "Plus_P_m": _business.get("PLUS", '  ')[1],
                               "Top_P": _business.get("TOP", '  ')[0], "Top_P_m": _business.get("TOP", '  ')[1]})
                _fight['url'] = f"{self.url}ida_vuelta=ida_vuelta&nadults=1&nchildren=0&ninfants=0&" \
                                f"fecha1_dia={self.params['dt_from'][-2:]}&" \
                                f"fecha1_anomes={self.params['dt_from'][:-3]}&"\
                                f"fecha2_dia={self.params['dt_to'][-2:]}&" \
                                f"fecha2_anomes={self.params['dt_to'][:-3]}&" \
                                f"from_city1={self.params['origin']}&to_city1={destination}#/"
                self._fights.writerow(_fight)
                self._total += 1
                del _economy, _business, _fight, _f, _r
        else:
            _r = self.session.get(
                url=f"{self.host}/ws/proxy/booking-webapp-bff/v1/public/revenue/recommendations/oneway",
                params=params).json().get("data", [{'flights': []}])
            sleep(self.time)
            if not _r[0]['flights'] == []:
                _f = _r[0]["flights"][0]
                _departure = _f["departure"]
                _arrival = _f["arrival"]
                _datetime1 = _departure["dateTime"].split("T")
                _datetime2 = _arrival["dateTime"].split("T")
                _fight.update({'cod': f'{self.params["origin"]} - {destination}',
                               '1_data_partida': _datetime1[0], '1_hora_partida': _datetime1[1],
                               '1_local_partida': f'{_departure["airportCode"]} - {_departure["airportName"]}'
                                                  f' - {_departure["countryCode"]}',
                               '1_data_chegada': _datetime2[0], '1_hora_chegada': _datetime2[1],
                               '1_local_chegada': f'{_arrival["airportCode"]} - {_arrival["airportName"]}'
                                                  f' - {_arrival["countryCode"]}'})
                del _departure, _arrival, _datetime1, _datetime2
                sleep(self.time)
                _f = _r[0]["flights"][0]["cabins"]
                if len(_f) > 1:
                    _economy = dict(
                        map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                       x['pricesPerPassenger']['adult']['currency'])), _f[0]['fares']))
                    _business = dict(
                        map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                       x['pricesPerPassenger']['adult']['currency'])), _f[1]['fares']))
                else:
                    if _f[0]['code'] == 'Y':
                        _economy = dict(
                            map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                           x['pricesPerPassenger']['adult']['currency'])),
                                _f[0]['fares']))
                        _business = {}
                    else:
                        _economy = {}
                        _business = dict(
                            map(lambda x: (x['category'], (x['pricesPerPassenger']['adult']['wholeTrip']['total'],
                                                           x['pricesPerPassenger']['adult']['currency'])),
                                _f[0]['fares']))

                _fight.update({"Promo_E": _economy.get("PROMO", '  ')[0], "Promo_E_m": _economy.get("PROMO", '  ')[1],
                               "Basic_E": _economy.get("BASIC", '  ')[0], "Basic_E_m": _economy.get("BASIC", '  ')[1],
                               "Light_E": _economy.get("LIGHT", '  ')[0], "Light_E_m": _economy.get("LIGHT", '  ')[1],
                               "Plus_E": _economy.get("PLUS", '  ')[0], "Plus_E_m": _economy.get("PLUS", '  ')[1],
                               "Top_E": _economy.get("TOP", '  ')[0], "Top_E_m": _economy.get("TOP", '  ')[1],
                               "Plus_P": _business.get("PLUS", '  ')[0], "Plus_P_m": _business.get("PLUS", '  ')[1],
                               "Top_P": _business.get("TOP", '  ')[0], "Top_P_m": _business.get("TOP", '  ')[1]})
                _fight['url'] = f"{self.url}ida_vuelta=ida&nadults=1&nchildren=0&ninfants=0&" \
                                f"fecha1_dia={self.params['dt_from'][-2:]}&" \
                                f"fecha1_anomes={self.params['dt_from'][:-3]}&"\
                                f"from_city1={self.params['origin']}&to_city1={destination}#/"
                self._fights.writerow(_fight)
                self._total += 1
                del _economy, _business, _fight, _f, _r

    def stalk_beta(self):
        start = time()
        try:
            self._import_csv(crawler='beta')
            list(map(self.info_markers, self.list_origin()))
        except Exception as e:
            print(e)
        finally:
            stop = time()
            return f"Concluido com sucesso\ntotal encontrado: {self._total}" \
                   f"Tempo de execução: {str(timedelta(seconds=stop - start)).split('.')[0]}"

    def stalk_stable(self):
        start = time()
        try:
            self._import_csv(crawler='stable')
            self.params["destiny"] = reduce(lambda x, y: x+y, map(self.list_origin, self.list_destiny()))
            list(map(self.info_markers_to_destination, self.list_origin(local='es_cl')))

        except Exception as e:
            print(e)
        finally:
            stop = time()
            return f"Concluido com sucesso\ntotal encontrado: {self._total}\n" \
                   f"Tempo de execução: {str(timedelta(seconds=stop-start)).split('.')[0]}"

    def stalk_filter(self, file):
        start = time()
        try:
            self._import_csv(crawler=f'filter_{file}')
            _filter = self.list_filter(file=file)
            if not _filter:
                return "Tente Novamente!!"
            self.params['destiny'] = _filter['destino']
            list(map(self.info_markers_to_destination, _filter['origem']))

        except Exception as e:
            print(e)
        finally:
            stop = time()
            return f"Concluido com sucesso\ntotal encontrado: {self._total}\n" \
                   f"Tempo de execução: {str(timedelta(seconds=stop - start)).split('.')[0]}"


if __name__ == "__main__":
    try:
        print("Consulta de passagens -> Latam Airlines Chile")

        _d1 = str(input("Partida data: ")).replace("/", "-").split("-")
        _d2 = str(input("Chegada data: " or None))
        option = int(input("Gostaria executar qual crawler[padrao=filter]: filter=0, stable=1, beta=2 -> ") or 0)
        if option == 0:
            _file = str(input("Nome do arquivo para fazer o filtro[padrao=all_national_to_filter]: ") or
                        "all_national_to_filter")
        _t = int(input("Time de requisição em seconds, altere o valor[padrao=0.1]: ") or 0)
        kwargs = {"dt_from": f"{_d1[-1]}-{_d1[-2]}-{_d1[-3]}"}
        _d = str(input("Deseja continuar de onde parou [s/n]: ") or "n")
        if 's' in _d.lower():
            kwargs['old'] = True
        if _d2:
            _d2 = _d2.replace("/", "-").split("-")
            kwargs['dt_to'] = f"{_d2[-1]}-{_d2[-2]}-{_d2[-3]}"

        print("Executando...")
        if option == 2:
            print(Latam(params=kwargs, tm=_t).stalk_beta())
        elif option == 1:
            print(Latam(params=kwargs, tm=_t).stalk_stable())
        else:
            print(Latam(params=kwargs, tm=_t).stalk_filter(file=_file))

    except IndexError:
        print("Formato de data incorreta, tente novamente!")
        print("Exemplo: 23-04-2020 ou 24/04/2020")
    except ValueError:
        print("Time de requisição ou Crawler, tente novamente")
        print("Exemplo: 0")
    sleep(60)
